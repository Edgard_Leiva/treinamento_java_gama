package exemplos;

import java.util.Scanner;

public class Exemplo03 {
    public static void main(String[] args) {
        Scanner teclado =  new Scanner(System.in);
        
        double nota, media = 0;
        int cont = 1;
        final int QTDE_ALUNOS = 3;
        
        while (cont <= QTDE_ALUNOS) {
                System.out.printf("Digite a nota do %d primeiro aluno:", cont);
                nota = teclado.nextDouble();
                media = media + nota;
                cont++;
        }

        media = media / QTDE_ALUNOS;
        System.out.println(" A média da turma é:" + media);

        teclado.close();

    }

}
