package exercicios;

import java.util.Scanner;

public class exercicio02 {
    public static void main(String[] args) {
        Scanner teclado =  new Scanner(System.in);

        int cont = 0, valor;
        
        System.out.println("digite o número da tabuada:");
        valor = teclado.nextInt();
        
            while ( cont <=10 ) {
            System.out.println("Resultado da Tabuada:" + valor * cont);
            cont++;    
        }

        teclado.close();
    }
    
}
