
/*
Um professor leciona em 5 turmas diferentes, e cada turma possui 20 alunos.
Escreva um programa que leia a média de cada aluno de cada uma das turmas, e exiba a média das notas por turma, e a média geral das turmas.

Dica: Teste com um número de alunos e turmas menor.

        for(L = 0; L < 4; L++) {
        for( c = 1; c < 4; c++) {
            println(L + " , " + c);
        }
         }
 

*/
package exercicios;
import java.util.Scanner;
public class exercicio07 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        final int QTDE_TURMAS = 2, QTDE_ALUNOS = 3;
        double mediaTurma = 0, mediaGeral= 0, mediaAluno;

        for (int turma = 1; turma <= QTDE_TURMAS; turma++) {
            System.out.println("Dados da turma " + turma);
            mediaTurma = 0;

            for (int aluno = 1; aluno <= QTDE_ALUNOS; aluno++) {
                System.out.println("Digite a média do aluno " + aluno + ":");
                mediaAluno = teclado.nextDouble();
                mediaTurma = mediaTurma + mediaAluno;
            }
            mediaTurma = mediaTurma / QTDE_ALUNOS;
            mediaGeral = mediaGeral + mediaTurma;
            System.out.printf("Média da turma %d: %.2f\n", turma, mediaTurma);
            
        }
        mediaGeral = mediaGeral / QTDE_TURMAS;
        System.out.printf("Média geral das turmas: %.2f\n", mediaGeral);

        teclado.close();
    }
}
