package exercicios;
import java.util.Scanner;

public class exercicio04 {
    public static void main(String[] args) {
        
        Scanner teclado =  new Scanner(System.in);
        final int qtde = 10;
        int numero, cont = 1;
        int pares=0, impares=0;

        while (cont <= qtde) {
            System.out.printf("Digite o %d numero:", cont);
            numero = teclado.nextInt();

            if (numero %2 == 0){
                pares++;
            } else {
                impares++;
            }
            cont++;
        }

        System.out.printf("Qtde de pares: %d \nQtde de impares: %d \n", pares, impares);
        teclado.close();
    }
}
