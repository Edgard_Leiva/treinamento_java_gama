package exercicios;

import java.util.Scanner;

public class exercicio05 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int numero = 1;
        int contador = 1;
        int soma = 0;

        while (numero!=0) {
            System.out.printf("Digite o %d numero:\n", contador);
            numero = teclado.nextInt();
            soma = soma + numero;
            contador++;
        }
        System.out.printf("A soma dos valores é:%d", soma);
        teclado.close();
    }
        

}
