
package exercicios;

import java.util.Scanner;

public class exercicio06 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int valor, soma = 0, pares = 0, impares = 0;
        final int qtde = 10;

        for (int i = 1; i <= qtde; i++) {
            System.out.printf("Digite o %d valor", i);
            valor = teclado.nextInt();

            if (valor % 2 == 0) {
                pares++;
                soma = soma + valor;
            } else {
                impares++;
            }

        }
        System.out.println("Média dos valores pares: " + ((double) soma / pares));
        System.out.println("% de numeros impares: " + (double) impares / qtde * 100 + "%");

        teclado.close();
    }
}