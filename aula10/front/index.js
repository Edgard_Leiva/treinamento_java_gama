function enviar() {
    let link = "http://localhost:8080/usuario/1";

    // fetch envia a requisição informada no link
    // .then -> executado somente depois que a resposta voltar do servidor
    fetch(link)
        .then(res => tratarResposta(res)); 
}

function enviarPost() {
    let cpfUser = "123123123";
    let senhaUser = "123";
    let link = "http://localhost:8080/usuario/login";

    let loginBody = {
        cpf: cpfUser,
        senha: senhaUser
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(loginBody),
        headers:{
            'Content-type':'application/json'
        }
    }

    fetch(link,cabecalho)
    .then(res => tratarResposta(res));
}

function tratarResposta(resposta) {
    if (resposta.status == 200) {
        console.log("Achei!");
        resposta.json().then( res => exibirDados(res));
    } else {
        console.log("Não achei...  :-(");
    }
    
}

function exibirDados(dados) {
    console.log(dados);
    document.getElementById("divResposta").innerHTML = dados.nome + ":" + dados.cpf;
        
}