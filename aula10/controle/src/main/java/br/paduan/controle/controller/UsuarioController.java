package br.paduan.controle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.paduan.controle.dao.UsuarioDAO;
import br.paduan.controle.model.Usuario;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin("*")
public class UsuarioController {
    @Autowired
    private UsuarioDAO dao;

    @GetMapping("/usuarios")
    public ResponseEntity<List<Usuario>> buscarTodos(){
        List<Usuario> lista = (List<Usuario>)dao.findAll();

        if(lista != null){
            return ResponseEntity.ok(lista);
        }else{
            return ResponseEntity.status(404).build();
        }

    }

    @PostMapping("/usuario/login/email")
    public ResponseEntity<Usuario> fazerLoginPorEmail(@RequestBody Usuario user) {
        Usuario usuario = dao.findByEmailAndSenha(user.getEmail(), user.getSenha());
        
        if(usuario != null){
            return ResponseEntity.ok(usuario);
        }else{
            return ResponseEntity.status(403).build();
        }
    }
    
    @PostMapping("/usuario/login")
    public ResponseEntity<Usuario> fazerLogin(@RequestBody Usuario user) {
        Usuario userEncontrado = dao.findByEmailOrCpf(user.getEmail(), user.getCpf());
        
        if(userEncontrado != null){
            if(userEncontrado.getSenha().equals( user.getSenha() )){
                userEncontrado.setChamados(null);
                userEncontrado.setSenha("*****");
                return ResponseEntity.ok(userEncontrado);
            }
        }

        return ResponseEntity.status(403).build();
    }

    @GetMapping("/usuario/{id}")
    public ResponseEntity<Usuario> buscarPorId(@PathVariable int id) {
        Usuario usuario = dao.findById(id).orElse(null);
        
        if(usuario != null){
            return ResponseEntity.ok(usuario);
        }else{
            return ResponseEntity.status(404).build();
        }
    }
    
}
