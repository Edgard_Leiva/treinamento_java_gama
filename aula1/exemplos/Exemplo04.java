package exemplos;

import java.util.Scanner;

public class Exemplo04 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String nome;

        System.out.println("Digite o seu nome:");
        nome=teclado.nextLine(); // guarda na variavel nome o que foi digitado no teclado
 
        System.out.println("Bem Vindo " + nome);

        teclado.close();
    }

    
}
