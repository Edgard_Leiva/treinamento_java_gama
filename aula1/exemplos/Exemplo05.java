package exemplos;

import java.util.Scanner;

public class Exemplo05 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int valor1, valor2, soma;

        System.out.println("Digite o primeiro Número:");
        valor1 = teclado.nextInt();

        System.out.println("Digite o segundo Número");
        valor2 = teclado.nextInt();

        soma = valor1 + valor2;

        System.out.println("A soma do valor é:" + soma);

        teclado.close();
    }
    
}
