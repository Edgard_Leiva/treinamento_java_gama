package exercicios;
import java.util.Scanner;
public class exercicio06 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        double val_pe, val_jarda, val_milha, km, pes, polegadas, jardas, milha;
        
        //pe = 12;
        //jarda = 3;
        //milha = 1760 jardas;
        //km = 0,62137 milhas;

        val_pe = 12;
        val_jarda = 3;
        val_milha = 1760;
        km = 0.62137;

        System.out.println("Digite o KM:");
        km = teclado.nextDouble();

        pes = (km * val_pe);
        polegadas = (val_pe * val_pe);
        jardas = (val_milha * val_milha);
        milha = (km * val_milha);

        System.out.println("Valor em Pes: " + pes);
        System.out.println("Valor em polegadas: " + polegadas);
        System.out.println("Valor em jardas: " + jardas);
        System.out.println("Valor em milhas: " + milha);
              
        teclado.close();

    }
    
}
