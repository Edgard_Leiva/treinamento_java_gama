package exercicios;

import java.util.Scanner;

public class exercicios02 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        double valor1, valor2, media;
        
        System.out.println("Insira nota1:" );
        valor1 = teclado.nextDouble();
        
        System.out.println("Insira nota2:" );
        valor2 = teclado.nextDouble();

        media = (valor1 + valor2)/2;

        System.out.println("Sua média é:" + media);

        teclado.close();

    }
    
}
