package exercicios;

import java.util.Scanner;

public class exercicio04 {
    public static void main(String[] args) {
        double salMinimo, qtde, total, fatura, desconto;

        Scanner teclado = new Scanner(System.in);

        System.out.println("Digite o salário Minimo");
        salMinimo = teclado.nextDouble();

        System.out.println("Digite o consumo de Quilowatts");
        qtde =  teclado.nextDouble();

        total = (qtde / 500);
        
        System.out.println("Total QuiloWatts consumidos: " + total);

        fatura = (total * qtde);

        System.out.println("Valor da Fatura: " + fatura);

        desconto = (fatura * 0.85);

        System.out.println("Fatura com desconto:" + desconto);
         
        
        teclado.close();

    }
    
}
