
package br.edgard.loja.controller;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// import br.edgard.loja.dao.PedidoControllerDAO;
import br.edgard.loja.dao.PedidoDAO;
import br.edgard.loja.model.Pedido;

/*
- listar todos os pedidos
- buscar um pedido pelo número do pedido
- listar os pedidos com valar maior que 500,00 reais
*/

@RestController
@CrossOrigin("*")
public class PedidoController {
    @Autowired
    private PedidoDAO dao;

    @GetMapping("/pedidos")
        public ResponseEntity<ArrayList<Pedido>> getpedidos(){
        ArrayList<Pedido> lista = (ArrayList<Pedido>) dao.findAll();
        
        if (lista !=null) {
            return ResponseEntity.ok(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/pedidos/numero/{id}")
        public ResponseEntity<Pedido> getPedidosPorNumero(@PathVariable int id){
        Pedido pedido = dao.findById(id).orElse(null);
        
        if (pedido !=null) {
            return ResponseEntity.ok(pedido);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

}   

   