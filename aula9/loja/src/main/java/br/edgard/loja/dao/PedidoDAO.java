package br.edgard.loja.dao;

import org.springframework.data.repository.CrudRepository;
import br.edgard.loja.model.Pedido;

public interface PedidoDAO extends CrudRepository<Pedido, Integer>{


}
    

