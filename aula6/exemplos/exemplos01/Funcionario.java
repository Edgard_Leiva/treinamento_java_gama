

package exemplos.exemplos01;

/**
 * Funcionario
 */

 //construtor
public class Funcionario {
    private String nome;
    private float salario;


    // metodos da classe
    // void - significa, metodo sem retorno  
    public Funcionario(String nome, float salario){
        this.nome = nome;
        this.salario = salario;

    }

    public void aumentarSalario(float perc){
        //salario = salario + salario * perc / 100; - igual a linha de baixo
        salario += salario * perc / 100;
                
    }
    // retorna uma String com resultado do seu processamento
    public String imprimir(){
        return "Funcionario: " + nome + "\nsalario: " +  String.format("%.2f\n", salario);

    }
}
    
