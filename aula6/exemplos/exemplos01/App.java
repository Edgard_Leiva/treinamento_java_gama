package exemplos.exemplos01;

public class App {
    public static void main(String[] args) {
        Funcionario func = new Funcionario("Armando", 1000);
        Gerente ger =  new Gerente("Rita", 2000, 10);

        func.aumentarSalario(10);
        System.out.println(func.imprimir());

        ger.aumentarSalario(10);
        System.out.println(ger.imprimir());
    }
    
}
