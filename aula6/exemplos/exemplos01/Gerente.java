package exemplos.exemplos01;

public class Gerente extends Funcionario {
    private int nFuncionarios;

    public Gerente(String nome, float salario, int nFuncionarios){
        super(nome, salario); // construtor da classe Funcionario
        this.nFuncionarios = nFuncionarios;

    }

    @Override // sobrescreve o método da classae base
    public void aumentarSalario(float perc) {
        super.aumentarSalario(perc + 20); // chama o aumentarSalario da classe base
    }

    @Override
    public String imprimir() {
        return super.imprimir() + "gerencia: " + nFuncionarios;
    }
}
