package exercicio.exercicios02;

public class Especial extends Conta {
    private double limite;

    public Especial(int numero, double limite){
        super(numero);
        this.limite = limite;
    }

    
    @Override
    public boolean sacar(double valor) {
        if( valor > 0 && getSaldo() + limite >= valor) {
            return super.sacar(valor);
        }
        return false;
    }
    
    @Override  
    public String exibir(){
        return super.exibir() + "limite:" + limite;
    }
    
    
}
