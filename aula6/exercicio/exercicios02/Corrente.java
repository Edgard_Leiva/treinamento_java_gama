package exercicio.exercicios02;

public class Corrente extends Conta {
    final double TAXA_DEPOSITO = 0.1;
    
    
    public Corrente(int numero){
        super(numero);
    }
    
    @Override
    public boolean sacar(double valor){
        if (getSaldo() >= valor){
            return super.sacar(valor);
        }
        return false;
    }

    @Override
    public void depositar(double valor){
        super.depositar(valor - TAXA_DEPOSITO);
    }

    
}
