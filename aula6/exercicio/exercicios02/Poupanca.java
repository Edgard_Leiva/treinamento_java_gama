package exercicio.exercicios02;

public class Poupanca extends Conta{
    private double taxa;

    public Poupanca(int numero){
        super(numero);
    }

    public double getTaxa(){
        return taxa;
    }

    public void setTaxa(double taxa){
        if (taxa > 0){
            this.taxa = taxa;
        }
    }

    @Override
    public boolean sacar(double valor) {
        if (valor > 0 && getSaldo() + taxa >= valor){
            return super.sacar(valor + taxa);
        }
        return false;
    }
}

    

