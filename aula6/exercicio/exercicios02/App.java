package exercicio.exercicios02;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
      //  Conta conta =  new Conta(1234);
       // Corrente cc =  new Corrente(22222);

        //conta.depositar(1000);
        //System.out.println(conta.exibir());

        //conta.sacar(2000);
        //System.out.println(cc.exibir());

        Scanner entrada = new Scanner(System.in);
        int opcao, numero;
        double valor, limite;
        ArrayList<Corrente> contas = new ArrayList<>();
        Corrente cc = null;
        Especial ce = null;


        do {
            System.out.println("1-Nova Conta Corrente");
            System.out.println("2-Nova Conta Especial");
            System.out.println("3-Nova Conta Poupança");
            System.out.println("4-Depositar");
            System.out.println("5-Sacar");
            System.out.println("6-Consultar Saldo");
            System.out.println("7-Sair");
            System.out.print("> ");
            opcao = entrada.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("Informe o número da conta:");
                    numero = entrada.nextInt();
                    cc = new Corrente(numero);
                    contas.add(cc);
                    break;
                case 2:
                    System.out.println("Informe o número da conta:");
                    numero = entrada.nextInt();
                    System.out.println("Informe o valor do limite:");
                    limite = entrada.nextDouble();
                    ce = new Especial(numero, limite);
                    break;
                case 3:
                    System.out.println("Informe o número da deposito:");
                    valor = entrada.nextDouble();
                    cc.depositar(valor);
                    break;
                case 4:
                    System.out.println("Informe o número da conta:");
                    numero = entrada.nextInt();
                    System.out.println("Informe o valor do depósito:");
                    valor = entrada.nextDouble();
                   
                    for (Corrente corrente : contas){
                        if( corrente.getNumero() == numero){
                            corrente.depositar(valor);
                        }
                    }
                    cc.depositar(valor);
                    break;
                case 5:
                    System.out.println("informe o valor do saque");
                    valor = entrada.nextDouble();
                    cc.sacar(valor);
                    break;
                case 6:
                    System.out.println("Informe o número da conta:");
                    numero = entrada.nextInt();

                    for(Corrente corrente : contas)
                    System.out.println(cc.exibir());
                    break;
                case 7:
                    break;

                default: // se não for nenhum dos outros casos
                    System.out.println("Opção inválida!");
                    break;
            }

        } while (opcao != 7);

        entrada.close();
    }

}
