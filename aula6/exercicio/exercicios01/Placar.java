package exercicio.exercicios01;

public class Placar {
    private int result1, result2;
    private String time1, time2;

    public Placar(){
        time1 = "casa";
        time2 = "visitante";
        result1 = 0;
        result2 = 0;
    }

    public Placar(String time1, String time2){
        this.time1 = time1;
        this.time2 = time2;
    }
    
    public Placar(String time1, String time2, int result1, int result2){
        this.time1 = time1;
        this.time2 = time2;
        this.result1 = result1;
        this.result2 = result2;
    }
    
    public String exibir(){
        return time1 + " " + result1 + " X " + time2 + " " + result2;
    }
   
}
