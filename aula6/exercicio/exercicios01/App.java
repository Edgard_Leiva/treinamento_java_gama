package exercicio.exercicios01;

public class App {
    public static void main(String[] args) {
        Placar timPlacar1 = new Placar();
        Placar timPlacar2 = new Placar("Sao Paulo", "Corinthians");
        Placar timPlacar3 = new Placar("Sao Paulo", "Corinthians", 3, 0);

        System.out.println( timPlacar1.exibir());
        System.out.println( timPlacar2.exibir());
        System.out.println( timPlacar3.exibir());


    }
    
}
