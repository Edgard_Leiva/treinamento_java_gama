package exemplos;

public class Pessoa {
    
    private String nome;
    private double salario;
    

    public Pessoa(String nome, double salario){
        this.nome = nome;
        this.salario = salario;
    }

    public void apresentar() {
        System.out.println(nome + ":" + salario);        
    }

    // Overload: pelo menos 1 parametro diferente
    public void apresentar(String titulo) {
        System.out.println("Ola sou " + titulo +" " + nome);
    }

}


