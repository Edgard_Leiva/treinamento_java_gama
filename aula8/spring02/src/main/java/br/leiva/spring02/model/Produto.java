package br.leiva.spring02.model;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity // está classe sera persistida no BD
@Table(name = "produto")
public class Produto {
    
    @Id //este campo será a chave primaria
    @GeneratedValue(strategy = GenerationType.IDENTITY) // campo gerado pelo BD sequencial
    @Column(name = "cod")
    private int codigo;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;
    private double valor;

    @Column(name = "valor")
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    

    
}
