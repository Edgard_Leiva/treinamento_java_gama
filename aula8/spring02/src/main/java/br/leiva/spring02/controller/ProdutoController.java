package br.leiva.spring02.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.leiva.spring02.dao.ProdutoDAO;
import br.leiva.spring02.model.Produto;

@RestController
@CrossOrigin("*") // aceita requisiçaõ de qq origem
public class ProdutoController {
    
    @Autowired //injeçaõ de dependencia - JPA criar classe
    private ProdutoDAO dao;

    @GetMapping("/produto/{id}")
    public ResponseEntity<Produto> getProduto(@PathVariable int id){
        Produto produto = dao.findById(id).orElse(null);

        if(produto != null) {
            return ResponseEntity.ok(produto);
        } else {
            return ResponseEntity.status(404).build();
        }
    }
    
    @GetMapping("/produtos")
    public ResponseEntity<ArrayList<Produto>> getTodosProdutos(){
        ArrayList<Produto> lista = (ArrayList<Produto>) dao.findAll();

        if(lista != null){
            return ResponseEntity.ok(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }
}
