
package br.leiva.spring02.dao;
import br.leiva.spring02.dao.ProdutoDAO;

import org.springframework.data.CrudRepository;

public class ProdutoDAO {
  
    public interface ProdutoDAO extends CrudRepository<Produto, Integer>{
    }
    
}
