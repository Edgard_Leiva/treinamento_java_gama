package br.leiva.spring01.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // indica que está classe sera um CONTROLLER REST
@RequestMapping("/exemplo") // indica que está rota é atendida nesta classe

public class SampleController {
    @GetMapping("/hello") // a requisição sera do tipo GET
    public ResponseEntity<String> getOla(){ //devolve uma resposta padrao WEB com um string
        return ResponseEntity.ok("ola Mundo!");  // status OK - 200
        
    }

    
}
