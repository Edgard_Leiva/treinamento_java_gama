package exemplos.exemplo01;

import java.util.ArrayList;
import java.util.HashSet;

public class Exemplo01{
    public static void main(String[] args) {
        HashSet<Produto> hashSet = new HashSet<>();

        ArrayList<Produto> array = new ArrayList<>();

        Produto p1 = new Produto(1,"Produto 1");
        Produto p2 = new Produto(2,"Produto 2");
        
        hashSet.add(p1);
        hashSet.add(p2);

        array.add(p1);
        array.add(p2);

        System.out.println(hashSet);
        System.out.println(array);

    }

}