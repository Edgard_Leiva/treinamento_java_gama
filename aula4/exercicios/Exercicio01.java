package exercicios;

import java.util.Scanner;

public class Exercicio01 {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite um numero:");
        int num = teclado.nextInt();

        boolean resposta = par(num);

        if (resposta) {
            System.out.println("é par");
        } else {
            System.out.println("é impar");
        }
        teclado.close();
    }

    static boolean par(int num){
        if ((num %2) == 0){
            return true;    
    }
    else{
        return false;
    }
    }
}
