package exercicios.exercicio03;

public class Classe {
    String modelo;
    String marca;
    double consumo;

    Classe(String marca, String modelo, double consumo){
    this.marca = marca;
    this.modelo = modelo;
    this.consumo = consumo;
    }

    void apresentar_veiculo(){
        System.out.println("Modelo:" + modelo + " Marca:" + marca);
    }

    void apresentar_consumo(){
        System.out.println("Consumo:" + consumo);
    }


  
}
