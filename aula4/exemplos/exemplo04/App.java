package exemplos.exemplo04;

public class App {
    public static void main(String[] args) {
        Pessoa p = new Pessoa("Carlos", 34);
        Pessoa p2 = new Pessoa("Ana", 30);

        p.apresentar();
        p2.apresentar();
    }
    
}
