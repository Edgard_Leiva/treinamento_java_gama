package exemplos;

public class Exemplo02 {
    public static void main(String[] args) {
        int resultado;
        
        soma(5, 7);
        
        resultado = soma2(5,7);
        System.out.println("A soma é:" + resultado);

        System.out.println("A soma é:" + soma2(2,3));

              
    }

    // este método recebe dois valores inteiros como parametro
    // este método não retorna nada: "void"

    static void soma(int n1, int n2){
        int resp;
        resp = n1 + n2;
        System.out.println("A soma vale:" + resp);
    }
    // "int" é o tipo de informaçã que este método me responde ao final da sua execução
    // cada metodo somente retorna um resultado
    
    static int soma2(int n1, int n2){
        int resp;

        resp = n1 + n2;

        return resp;

    }

}
