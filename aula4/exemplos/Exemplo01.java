package exemplos;

/**
 * Exemplo01
 */
public class Exemplo01 {

    public static void main(String[] args) {
        linha();
        System.out.println("Meu programa");
        linha();
        System.out.println("Seja bem vindo ao programa");
        linha2(27);
        linha3(20, '*');
    }

    static void linha(){
        System.out.println("-----------");
    }
    

    static void linha2(int tam){
        for (int i = 0; i < tam; i++){
            System.out.print("-");
        }
    }

    static void linha3(int tam, char tipo){
        for (int i = 0; i < tam; i++){
            System.out.print(tipo);
        }
    }

}   