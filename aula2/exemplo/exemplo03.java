package exemplo;

import java.util.Scanner;

public class exemplo03 {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        double nota;

        System.out.println("Digite a nota:");
        nota = teclado.nextDouble();

        if (nota >= 6) {
            System.out.println("Aprovado");
        }

        teclado.close();
    }
}