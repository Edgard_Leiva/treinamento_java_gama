package uri;

import java.util.Scanner;

/*
 * ex1003
 */
public class ex1003 {
    public static void main(String[] args) {
        int a, b, soma;

        Scanner teclado = new Scanner(System.in);
        
        //System.out.println("Valor A");
        a = teclado.nextInt();
        b = teclado.nextInt();

        soma = (a + b);

        System.out.println("SOMA = " + soma);

        teclado.close();
    }
    
}