package exercicio;

import java.util.Scanner;

public class exercicio06 {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);

        double salario;

        System.out.println("Digite o salario:");
        salario = teclado.nextDouble();

        if (salario <= 600) {
            System.out.println("Isento");
        } else {
            if (salario <= 1200) {
                System.out.println("Cobrar imposto de 20%");
            } else {
                if (salario <= 2000) {
                    System.out.println("Cobrar imposto de 25%");
                } else {
                    System.out.println("Será cobrado 30%");
                }
            }
        }
        teclado.close();
    }

}
