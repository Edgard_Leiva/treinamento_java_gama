package exercicio;

import java.util.Scanner;

public class exercicio05 {
    public static void main(String[] args) {
        String senha, senhafixa;

        Scanner teclado = new Scanner(System.in);

        System.out.println("Digite a senha:");
        senha = teclado.nextLine();

        senhafixa = "R10p5";

        if (senha.equals(senhafixa)) {
            System.out.println("Acesso Concedido");
        } else {
            System.out.println("Acesso negado");

        }

        teclado.close();

    }

}
