package exercicio;

import java.util.Scanner;
/**
 * exercicio01
 */
public class exercicio01 {

    public static void main(String[] args) {
        int numero;
        Scanner entrada = new Scanner(System.in);

        System.out.println("Digite o número inteiro:");
        numero = entrada.nextInt(); 


        if (numero > 20) {
            System.out.println("Metade: " + (double) numero /2);
        }
        
        entrada.close();

    }
}